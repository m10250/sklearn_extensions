import pandas as pd
import cv2
from sklearn.base import BaseEstimator, TransformerMixin
import numpy as np
from skimage.feature import hog
from skimage import color
from skimage import io
from skimage.metrics import structural_similarity
from PIL import Image
from joblib import Parallel, delayed, Memory
import multiprocessing
from tqdm import tqdm
tqdm.pandas()
#https://gist.github.com/soruly/bd02a218690fe4e19295de3f5bede242
# https://www.docs.opencv.org/master/dc/dc3/tutorial_py_matcher.html

# TODO implement n_jobs
class ImageSimiliarity(BaseEstimator, TransformerMixin):
    """
    Detects similar images based on SIFT Features

    Column: Contains the image names
    Path: Path to the images
    limit_points: Maximum number of points to consider
    distance_factor: Threshold untill which distance points are considered equal

    Size = Size to which images are resized



    """

    def __init__(self,column,path=None, type="SIFT", compare_images_path=list, size=(256,256), limit_points=50, distance_factor=0.75, progress=True, n_jobs=1, cache=None, speedup=True):
        self.column=column
        self.path=path
        self.type=type
        self.progress=progress
        self.n_jobs=n_jobs
        self.limit_points=limit_points
        self.compare_images = compare_images_path
        self.size = size
        self.distance_factor = distance_factor

        self.speedup = speedup
        if self.speedup == True:
            cv2.setUseOptimized(True)

        if cache is not None:
            memory = Memory(cache, verbose=0)
            self.cache = cache
            self.transform=memory.cache(self.transform)

    def fit(self, X, y=None):
        results=[]
        if self.progress == True:
            t=tqdm(self.compare_images)
        else:
            t=self.compare_images
        for path in t:
            img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
            rescaled_image = cv2.resize(img, self.size)
            results.append(rescaled_image)

        self.images = results
        self.column_names = ["Surf_Similarity_" +str(i) for i in range(len(self.images))]
        return self

    def transform(self, X, y=None):
        def compare_images(img, compare_images):
            image = cv2.imread(img, cv2.IMREAD_GRAYSCALE)
            image_resized = cv2.resize(image, self.size)
            _, representation = cv2.SIFT_create().detectAndCompute(image_resized, None)
            results = []
            for example in compare_images:
                _, example_representation = cv2.SIFT_create().detectAndCompute(example, None)
                bf = cv2.BFMatcher()
                matches = bf.knnMatch(representation, example_representation, k=2)
                #matches = bf.match(representation, example_representation)
                matches = sorted(matches, key=lambda x: x[0].distance)
                matches = matches[:self.limit_points]
                good = []
                for m, n in matches:
                    if m.distance < self.distance_factor * n.distance:
                        good.append([m])
                results.append(len(good))
            return results

        X_data = X.copy()
        X_data = X_data[self.column].copy()

        if self.path is not None:
            X_data = self.path + X_data
        if self.progress == True:
            #print("Calculate Similiarities based on SURF")
            results = [compare_images(i[0], self.images) for i in tqdm(X_data[self.column].values, desc="Image similarity")]
        else:
            results = [compare_images(i[0], self.images) for i in X_data[self.column].values]
        results = pd.DataFrame(results, columns=self.column_names, index=X_data.index)
        return results

    def get_feature_names(self):
        return self.column_names
    
    def get_feature_names_out(self):
        """get feature names of transform for sklearn compatibility > 1.0.0 """
        return self.column_names


class CompareSSIM(BaseEstimator, TransformerMixin):
    """
    Compare images based on SSIM (-1,1) -> 0 no correlation at all, 1 perfect match.

    Column: Contains the image names
    Path: Path to the images
    Size = Size to which images are resized
    Name: Name of return columns

    """
    def __init__(self,column,path=None, compare_images_path=list, size=(256,256), progress=True, n_jobs=1, name="SSIM", cache=None, speedup=True):
        self.column=column
        self.path=path
        self.progress=progress
        self.n_jobs=n_jobs
        self.compare_images_path = compare_images_path
        self.name = name
        self.size=size

        self.speedup = speedup
        if self.speedup == True:
            cv2.setUseOptimized(True)

        if cache is not None:
            memory = Memory(cache, verbose=0)
            self.cache = cache
            self.transform=memory.cache(self.transform)
        else:
            self.cache = cache

    def fit(self, X, y=None):
        results=[]
        if self.progress == True:
            t=tqdm(self.compare_images_path)
        else:
            t=self.compare_images_path
        for path in t:
            img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
            img_resize = cv2.resize(img, self.size)
            results.append(img_resize)

        self.images = results
        column_names = []

        for c in range(len(self.images)):
            column_names.append(self.name +"_img_" + str(c + 1))
        self.column_names = column_names
        return self

    def transform(self, X, y=None):
        def compare_images(img, compare_images):
            image = cv2.imread(img, cv2.IMREAD_GRAYSCALE)
            img_resize = cv2.resize(image, self.size)
            return_value = []
            return_images = []
            for temp_img in compare_images:
                (value, diff_img) = structural_similarity(img_resize, temp_img, full=True)
                return_value.append(value)
                return_images.append(diff_img)
            return return_value, return_images

        X_data = X.copy()
        X_data = X_data[self.column].copy()

        if self.path is not None:
            X_data = self.path + X_data
        if self.progress == True:
            #print("Calculate Similiarities based on SSIM")
            results = Parallel(n_jobs=self.n_jobs)(delayed(compare_images)(i[0], self.images) for i in tqdm(X_data[self.column].values, desc="SSIM"))
        else:
            #take the 0th element of the series
            results = Parallel(n_jobs=self.n_jobs)(
                delayed(compare_images)(i[0], self.images) for i in X_data[self.column].values)

        r = pd.DataFrame([i[0] for i in results], columns=self.column_names, index=X_data.index)
        self.image_diff = np.array([i[1] for i in results])
        return r

    def get_feature_names(self):
        return self.column_names
    
    def get_feature_names_out(self):
        """get feature names of transform for sklearn compatibility > 1.0.0 """
        return self.column_names


