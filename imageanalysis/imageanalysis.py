import pandas as pd
import cv2
from sklearn.base import BaseEstimator, TransformerMixin
import numpy as np
from skimage.feature import hog
from skimage import color, transform
from skimage.color import rgb2hsv
from skimage import io
from skimage import filters
from skimage.morphology import disk, square
from PIL import Image
from joblib import Parallel, delayed, Memory
import multiprocessing
from tqdm import tqdm
from sklearn.feature_extraction.image import extract_patches_2d
tqdm.pandas()

# https://gist.github.com/psinger/6e5f11981588378bc9316397131be66a
class HOGdetection(BaseEstimator, TransformerMixin):
    """
    Transformer for Hog Detection
    Based on sklearn
    RGB: Used if colored images are used

    Ready for multiprocessing

    Column: Contains the image names
    Path: Path to the images
    size: Image Size to resize to
    orientiatons: number of bins for HOG
    pixels_per_cell: pixel per cell
    cells_per_block: Number of cells for normalization
    RGB: Read RGB version of image
    progress: Display progressbar
    n_jobs: Number of jobs to use
    cache: Cache dir
    """

    def __init__(self, column,path=None,size=(256,256), orientations=9, pixels_per_cell=(8,8), cells_per_block=(3,3), RGB=True, progress=True, n_jobs=1, cache=None):
        self.path = path
        self.column = column
        self.orientations=orientations
        self.pixels_per_cell=pixels_per_cell
        self.cells_per_block=cells_per_block
        self.RGB = RGB
        self.progress = progress
        self.n_jobs=n_jobs
        self.size = size
        #if n_jobs==-1:
        #    self.n_jobs=multiprocessing.cpu_count()

        if cache is not None:
            memory = Memory(cache, verbose=0)
            self.cache = cache
            self.transform=memory.cache(self.transform)
        else:
            self.cache = cache

    def _hogdetection(self,img_path, orientations, cells_per_block, pixels_per_cell, multichannel):
        """
        Transform a single image to hog
        :param img_path:
        :return: hog
        """
        if self.RGB == False:
            image = color.rgb2gray(io.imread(img_path))
            size = (self.size[0], self.size[1],1)
        else:
            image = Image.open(img_path)
            size = (self.size[0], self.size[1], 3)

        resized = transform.resize(np.array(image),size)
        #hogs = hog(resized, orientations=self.orientations, cells_per_block=self.cells_per_block,
        #           pixels_per_cell=self.pixels_per_cell, multichannel=self.RGB)
        
        hogs = hog(resized, orientations=orientations, cells_per_block=cells_per_block,
                   pixels_per_cell=pixels_per_cell, multichannel=multichannel)



        return hogs

    def fit(self, X,y=None):
        X_data = X.copy()
        X_data = X_data[self.column].copy()
        if self.path == None:
            path = X_data.iloc[0,0]
        else:
            path = self.path+X_data.iloc[0]
        temp = self._hogdetection(path, orientations=self.orientations,cells_per_block=self.cells_per_block , pixels_per_cell=self.pixels_per_cell, multichannel=self.RGB)
        self.column_names = ["HOG_" + str(i) for i in range(len(temp))] # Calculate the number of features
        return self

    def transform(self, X):
        X_data = X.copy()
        X_data = X_data[self.column].copy()

        if self.path is not None:
            X_data = self.path+X_data
        if self.progress == True:
            #print("HOG in Progress")
            results = Parallel(n_jobs=self.n_jobs)(delayed(self._hogdetection)(i[0], orientations=self.orientations,cells_per_block=self.cells_per_block , pixels_per_cell=self.pixels_per_cell, multichannel=self.RGB) for i in tqdm(X_data[self.column].values, desc="HOG Analysis"))
        else:
            results = Parallel(n_jobs=self.n_jobs)(
                delayed(self._hogdetection)(i[0], orientations=self.orientations,cells_per_block=self.cells_per_block , pixels_per_cell=self.pixels_per_cell, multichannel=self.RGB) for i in X_data[self.column].values) #take the 0th element of the series

        results = pd.DataFrame(results, columns=self.column_names, index=X_data.index)
        return results

    def get_feature_names(self):
        return self.column_names
    
    def get_feature_names_out(self):
        """get feature names of transform for sklearn compatibility > 1.0.0 """
        return self.column_names


class EdgeDetection(BaseEstimator, TransformerMixin):
    """
        Transformer for Canny Edge Detection
        Column: Contains the image names
        Path: Path to the images
        Upper, Lower threshold for the edge detection for possible and true edges need to be set
        flatten: Return the results flattend
        progress: Display progress bar
        n_jobs = number of jobs
        cache = Cache dir
        speedup: Use speedup version of CV2
        size = size of the images as tuple
        """

    def __init__(self, column,size, lowerThreshold, upperThresold,flatten=True,path=None,progress=True, n_jobs=1, cache=None, speedup=True):
        self.lowerThreshold=lowerThreshold
        self.upperThresold=upperThresold
        self.path = path
        self.column = column
        self.flatten = flatten
        self.progress=progress
        self.n_jobs=n_jobs
        self.size=size
        if n_jobs==-1:
            self.n_jobs=multiprocessing.cpu_count()

        self.speedup = speedup
        if self.speedup == True:
            cv2.setUseOptimized(True)

        if cache is not None:
            memory = Memory(cache, verbose=0)
            self.cache = cache
            self.transform=memory.cache(self.transform)

    def _edgedetection(self,img_path, thresold1, threshold2):
        # threshold 1 lower threshold -> possible edge
        # threshold 2 upper threshold -> true edge
        image = cv2.imread(img_path, 0)  # read images as black and white
        resize = cv2.resize(image, self.size)
        edges = cv2.Canny(resize, thresold1, threshold2)

        return edges

    def fit(self, X,y=None):
        X_data = X.copy()
        X_data = X_data[self.column].copy()

        if self.path is not None:
            X_data = self.path + X_data

        r = self._edgedetection(X_data.iloc[0,0], self.lowerThreshold, self.upperThresold)
        self.column_names=["Pixel_" + str(i) for i in range(self.size[0]*self.size[1])]
        return self

    def transform(self, X):
        X_data = X.copy()
        X_data = X_data[self.column].copy()

        if self.path is not None:
            X_data = self.path+X_data

        if self.progress==True:
            #print("Edge detection in Progress")
            results = Parallel(n_jobs=self.n_jobs)(
                delayed(self._edgedetection)(i[0], self.lowerThreshold, self.upperThresold) for i in tqdm(X_data[self.column].values, desc="Edge detection"))
        else:
           #take the 0th element of the series
            results = Parallel(n_jobs=self.n_jobs)(
                delayed(self._edgedetection)(i[0], self.lowerThreshold, self.upperThresold) for i in X_data[self.column].values)
        results = np.array(results)

        if self.flatten==True:
            results = np.reshape(results,(len(X_data),self.size[0]*self.size[1]))
            results = pd.DataFrame(results, columns=self.column_names, index=X_data.index)
        return results


    def get_feature_names(self):
        return self.column_names
    
    def get_feature_names_out(self):
        """get feature names of transform for sklearn compatibility > 1.0.0 """
        return self.column_names


class ColorAnalysis(BaseEstimator, TransformerMixin):
    """
    Transfomer for Color Analysis

    Column: Contains the image names
    Path: Path to the images

    colorspace is a dictionary; Key is colorname; value: list with HSV values  --> tuples (low, high)
    H:[0,180], S:[0,255], V:[0,255]

    patchsize: Analyse color per patch
    """

    # https://stackoverflow.com/questions/10948589/choosing-the-correct-upper-and-lower-hsv-boundaries-for-color-detection-withcv
    def __init__(self, column, path, colorspace=dict, progress=True, n_jobs=1, cache=None, speedup=True, patchsize=None, size=None):
        #expects color dictionary
        self.column=column
        self.path = path
        self.colorspace=colorspace
        self.progress=progress
        self.n_jobs=n_jobs
        self.patchsize=patchsize
        self.size=size
        if self.patchsize!=None:
            self.strides = (np.ceil(size[0] / patchsize[0]), np.ceil(size[1] / patchsize[1]))

        self.speedup = speedup
        if self.speedup == True:
            cv2.setUseOptimized(True)

        if cache is not None:
            memory = Memory(cache, verbose=0)
            self.cache = cache
            self.transform=memory.cache(self.transform)

    def fit(self, X=None, y=None):
        if self.patchsize == None:
            self.column_names = list(self.colorspace.keys())
        else:
            names = []
            #num_patches = self.patchsize[0]*self.patchsize[1]
            for c in self.colorspace.keys():
                for num_patch in range(0, int(self.strides[0]*self.strides[1])):
                    names.append(str(c)+"_"+str(num_patch))
            self.column_names = names
        return self

    def analyse_color(self, img, colorspace):
        #cv2.setUseOptimized(True)
        image = cv2.imread(img)
        if self.size!=None:
            image_resize=cv2.resize(image, self.size)
            image = image_resize
        image_hsv=cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        return_results = []

        for key in colorspace.keys():
            params = colorspace[key]
            if self.patchsize==None:
                #params = colorspace[key]
                results = np.zeros((image_hsv.shape[0], image_hsv.shape[1]))
                h_condition = np.where((image_hsv[:, :, 0] >= params[0][0]) & (image_hsv[:, :, 0] <= params[0][1]), 1, 0)
                s_condition = np.where((image_hsv[:, :, 1] >= params[1][0]) & (image_hsv[:, :, 1] <= params[1][1]), 1, 0)
                v_condition = np.where((image_hsv[:, :, 2] >= params[2][0]) & (image_hsv[:, :, 2] <= params[2][1]), 1, 0)

                results[np.where((h_condition == True) & (s_condition == True) & (v_condition == True))] = 1
                return_results.append(results.sum() / results.size)
            else:
                for jj in range(int(self.strides[0])):
                    for kk in range(int(self.strides[1])):
                        height_begin = jj * int(self.strides[0])
                        height_end = height_begin + int(self.strides[0])

                        width_begin = kk * int(self.strides[1])
                        width_end = width_begin + int(self.strides[1])

                        p = image_hsv[height_begin:height_end, width_begin:width_end]

                        results = np.zeros((p.shape[0], p.shape[1]))
                        h_condition = np.where((p[:, :, 0] >= params[0][0]) & (p[:, :, 0] <= params[0][1]), 1,
                                       0)
                        s_condition = np.where((p[:, :, 1] >= params[1][0]) & (p[:, :, 1] <= params[1][1]), 1,
                                       0)
                        v_condition = np.where((p[:, :, 2] >= params[2][0]) & (p[:, :, 2] <= params[2][1]), 1,
                                       0)

                        results[np.where((h_condition == True) & (s_condition == True) & (v_condition == True))] = 1
                        return_results.append(results.sum() / results.size)

        return return_results

    def transform(self, X):

        X_data = X.copy()
        X_data = X_data[self.column].copy()

        if self.path is not None:
            X_data = self.path + X_data
        if self.progress == True:
            print("Color Analysis in Progress")
            results = Parallel(n_jobs=self.n_jobs, prefer="threads")(delayed(self.analyse_color)(i[0], self.colorspace) for i in tqdm(X_data[self.column].values, desc="Coloranalysis"))
        else:
            #take the 0th element of the series
            results = Parallel(n_jobs=self.n_jobs, prefer="threads")(delayed(self.analyse_color)(i[0], self.colorspace) for i in X_data[self.column].values)
        #all_results = pd.DataFrame(results, columns=self.colorspace.keys(), index=X_data.index)
        all_results = pd.DataFrame(results, columns=self.column_names, index=X_data.index)
        return all_results


    def get_feature_names(self):
        return self.column_names
    
    def get_feature_names_out(self):
        """get feature names of transform for sklearn compatibility > 1.0.0 """
        return self.column_names

class calcEntropy(BaseEstimator, TransformerMixin):
    """
    Calculate the entropy of an image

    Column: Contains the image names
    Path: Path to the images
    Size = Size to which images are resized
    Patch_size = Patch Size that is considered
    footprint = footprint used to calculate the entropy
    Name: Name of return columns
    overall: Calculate also overall/total entropy
    """
    def __init__(self,column,path=None, size=(256,256), patch_sizes=(16,16), footprint=[disk(10)], overall=True, progress=True, n_jobs=1, name="Entropy", cache=None, speedup=True):
        self.column=column
        self.path=path
        self.progress=progress
        self.n_jobs=n_jobs
        self.name = name
        self.size=size
        self.footprint = footprint
        if patch_sizes != None:
            self.patch_sizes = patch_sizes
            self.strides = (np.ceil(size[0]/patch_sizes[0]), np.ceil(size[1]/patch_sizes[1]))
        else:
            self.patch_sizes=patch_sizes
            self.strides = (0,0)

        self.overall = overall

        self.speedup = speedup

        if self.speedup == True:
            cv2.setUseOptimized(True)

        if cache is not None:
            memory = Memory(cache, verbose=0)
            self.cache = cache
            self.transform=memory.cache(self.transform)
        else:
            self.cache = cache


    def read_image(self, path):
        image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
        img_resize = cv2.resize(image, self.size)
        return img_resize

    def calc_entropy(self,img):
        image = cv2.imread(img, cv2.IMREAD_GRAYSCALE)
        resized_image = cv2.resize(image, self.size)

        if self.patch_sizes is not None:
            results = []
            for jj in range(int(self.strides[0])):
                for kk in range(int(self.strides[1])):
                    height_begin = jj*int(self.strides[0])
                    height_end = height_begin+int(self.strides[0])

                    width_begin = kk * int(self.strides[1])
                    width_end = width_begin + int(self.strides[1])

                    p=resized_image[height_begin:height_end, width_begin:width_end]
                    entropy_img = filters.rank.entropy(p, self.footprint[0])
                    return_value = np.array(entropy_img).flatten().mean()
                    results.append(return_value)
            return results
        else:
            img_resize = resized_image
            entropy_img = filters.rank.entropy(img_resize, self.footprint[0])
            return_value = np.array(entropy_img).flatten()
            return return_value

    def fit(self, X, y=None):
        img=X[self.column].iloc[0]
        if self.path is not None:
            img_path = self.path + img
            img_path = img_path.values[0]
        else:
            img_path = img.values[0]
        entropy = self.calc_entropy(img_path)
        column_names = [self.name+"_"+str(i+1) for i in range(len(entropy))]
        if self.overall == True:
            column_names.append("OverallEntropy")
        self.column_names = column_names
        return self

    def transform(self, X, y=None):
        X_data = X.copy()
        X_data = X_data[self.column].copy()

        if self.path is not None:
            X_data = self.path + X_data
        if self.progress == True:
            print("Calculate Entropy")
            results = Parallel(n_jobs=self.n_jobs)(delayed(self.calc_entropy)(i[0]) for i in tqdm(X_data[self.column].values, desc="Entropy"))
        else:
            results = Parallel(n_jobs=self.n_jobs)(
                delayed(self.calc_entropy)(i[0]) for i in X_data[self.column].values)

        if self.overall == True:
            r = pd.DataFrame(results, columns=self.column_names[:-1], index=X_data.index)
            r["OverallEntropy"]=np.array(results).mean(axis=1)
        else:
            r = pd.DataFrame(results, columns=self.column_names, index=X_data.index)
        return r

    def get_feature_names(self):
        return self.column_names
    
    def get_feature_names_out(self):
        """get feature names of transform for sklearn compatibility > 1.0.0 """
        return self.column_names
