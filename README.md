# Sklearn Extensions

Helpful extensions for Sklearn 

This repo includes helpful sklearn features:


- Use pandas backbone for functions to main column names
- Statsmodel linear regression integration 
- Useful dummycoder, ordinal scaler, etc. 
- Integreation of textanalysis features 
