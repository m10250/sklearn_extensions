from gensim.models.doc2vec import TaggedDocument
import pandas as pd
from sklearn.base import TransformerMixin, BaseEstimator
import numpy as np
import gensim.downloader
from joblib import Parallel, delayed, Memory
from tqdm import tqdm
from nltk.tokenize import sent_tokenize, word_tokenize
from six import string_types
import gensim
from gensim.models.doc2vec import Doc2Vec
from gensim.models.word2vec import Word2Vec


def check_version():
    if int(gensim.__version__[0]) >= 4:
        GENSIM_VERSION_4 = True
    else:
        GENSIM_VERSION_4 = False
    return GENSIM_VERSION_4

VERSION = check_version()

if VERSION == True:
    try:
        from gensim_4_wrapper import * 
    except:
        try:
            from sklearn_extensions.textanalysis.gensim_4_wrapper import *
        except:
            pass
else:
    from gensim.sklearn_api import W2VTransformer, D2VTransformer

class w2Vec(BaseEstimator, TransformerMixin):
    def __init__(self, model=W2VTransformer(), names=None, column_name="Text", cache=None, progress_bar=True,
                 n_jobs=1):  # model=Word2Vec()

        self.model = model
        self.names = names
        self.progress_bar = progress_bar
        self.n_jobs = n_jobs
        self.column_name = column_name
        self.GENSIM_VERSION_4 = check_version()

        if cache is not None:
            memory = Memory(cache, verbose=0)
            self.cache = cache
            self.transform = memory.cache(self.transform, ignore=['self'])
        else:
            self.cache = None

    def fit(self, X, y=None):
        X_data = X[self.column_name].copy()
        texts = [text.split(" ") for text in X_data]
        self.fit_model = self.model.fit(texts)

        if self.GENSIM_VERSION_4 == True:
            size = self.fit_model.vector_size
        else:
            size = self.fit_model.size

        if self.names == None:
            self.columns = ["Embedding_" + str(i + 1) for i in range(size)] # self.fit_model.vector_size #.size for 3.8.3
        else:
            self.columns = [self.names + str(i + 1) for i in range(size)] # #.size for 3.8.3
        return self

    def _get_score(self, input):
        nwords = 1

        if self.GENSIM_VERSION_4 == True:
            size = self.fit_model.vector_size
        else:
            size = self.fit_model.size

        feature_vec = np.zeros(size, dtype="float32") #self.fit_model.vector_size,
        for word in input:
            try:
                predict_vec = self.fit_model.transform(word)[0]
                nwords = nwords + 1
                feature_vec = np.add(feature_vec, predict_vec)
            except:
                pass
        feature_vec = np.divide(feature_vec, nwords)
        return feature_vec


    def transform(self, X):
        X_data = X[self.column_name].copy()
        texts = [text.split(" ") for text in X_data]

        if self.progress_bar == True:
            results = Parallel(n_jobs=self.n_jobs)(delayed(self._get_score)(i) for i in tqdm(texts, desc="Inferring Word2Vec"))
        else:
            results = Parallel(n_jobs=self.n_jobs)(delayed(self._get_score)(i) for i in texts)
        results = pd.DataFrame(results, index=X_data.index, columns=self.get_feature_names())
        return results

    def get_feature_names(self):
        return self.columns

    def get_most_similar_word(self, X, topn=10):
        """
        :param X: Word that is been looked up
        :param topn: Number of words returned
        :return:
        """
        if self.GENSIM_VERSION_4 == True:
            returns = self.fit_model.gensim_model.wv.most_similar(X, topn=topn)
        else:
            returns = self.fit_model.gensim_model.most_similar(X, topn=topn)
        return returns

    def get_most_similar_example(self, postive=None, negative=None, topn=10):
        """

        :param postive: Positive words
        :param negative: negative words
        :param topn: Top n words returned
        :return:
        """
        if self.GENSIM_VERSION_4 == True:
            returns = self.fit_model.gensim_model.wv.most_similar(postive, negative, topn=topn)
        else:
            returns = self.fit_model.gensim_model.most_similar(postive, negative, topn=topn)
        return returns
        #returns = self.fit_model.gensim_model.most_similar(postive, negative, topn)
        return returns

    def get_word_doesnot_match(self, X):
        """
        Returns word that does not match here
        :param X: Strings
        :return:
        """
        str_ = X.copy().split()
        #returns = self.fit_model.gensim_model.doesnt_match(str_)
        if self.GENSIM_VERSION_4 == True:
            returns = self.fit_model.gensim_model.wv.doesnt_match(str_)
        else:
            returns = self.fit_model.gensim_model.doesnt_match(str_)
        return returns


class d2Vec(BaseEstimator, TransformerMixin):
    def __init__(self, model=D2VTransformer(), names=None, column_name="Text", tokenize_input=True, cache=None,
                 progress_bar=True, n_jobs=1, **kwargs):
        self.model = model
        self.names = names
        self.progress_bar = progress_bar
        self.n_jobs = n_jobs
        self.column_name = column_name
        self.tokenize_input = tokenize_input
        self.GENSIM_VERSION_4 = check_version()

        if cache is not None:
            memory = Memory(cache, verbose=0)
            self.cache = cache
            self.transform = memory.cache(self.transform, ignore=['self'])
        else:
            self.cache = None

    def fit(self, X, y=None):
        X_data = X[self.column_name].copy()
        doc2vec = self.model
        #tagged_documents = [TaggedDocument(doc, [i]) for i, doc in enumerate(X_data)]
        tagged_documents = [TaggedDocument(words, [i]) for i, words in enumerate(X_data)]

        self.fit_model = self.model.fit(tagged_documents)

        if self.GENSIM_VERSION_4 == True:
            size = self.fit_model.vector_size
        else:
            size = self.fit_model.size

        if self.names == None:
            self.columns = ["DocEmbedding_" + str(i + 1) for i in range(size)] #.size for 3.8.3
        else:
            self.columns = [self.names + str(i + 1) for i in range(size)] # self.fit_model.vector_size #.size for 3.8.3
        return self

    def _get_score(self, input):
        return self.fit_model.transform(input)[0] # select 0th element

    def transform(self, X):
        X_data = X[self.column_name].copy()
        if self.tokenize_input == True:
            X_data = [word_tokenize(t) for t in X_data]  # sent_tokenize(X_data)
        # TODO tokenize the string first ...
        # results= [self.fitted_doc2vec.infer_vector(text) for text in X_data] # old
        #print(X_data)
        if self.progress_bar == True:
            results = Parallel(n_jobs=self.n_jobs)(
                delayed(self._get_score)(i) for i in tqdm(X_data, desc="Inference of Doc2Vec"))
        else:
            results = Parallel(n_jobs=self.n_jobs)(delayed(self._get_score)(i) for i in X_data)
        results = pd.DataFrame(results, columns=self.get_feature_names(), index=X.index)
        return results

    def get_feature_names(self):
        return self.columns

    def get_most_similar_word(self, X, topn=10):
        """
        :param X: Word that is been looked up
        :param topn: Number of words returned
        :return:
        """
        if self.GENSIM_VERSION_4 == True:
            returns = self.fit_model.gensim_model.wv.most_similar(X, topn=topn)
        else:
            returns = self.fit_model.gensim_model.most_similar(X, topn=topn)
        return returns

    def get_most_similar_example(self, postive=None, negative=None, topn=10):
        """

        :param postive: Positive words
        :param negative: negative words
        :param topn: Top n words returned
        :return:
        """
        if self.GENSIM_VERSION_4 == True:
            returns = self.fit_model.gensim_model.wv.most_similar(postive, negative, topn=topn)
        else:
            returns = self.fit_model.gensim_model.most_similar(postive, negative, topn=topn)
        return returns
        #returns = self.fit_model.gensim_model.most_similar(postive, negative, topn)
        return returns

    def get_word_doesnot_match(self, X):
        """
        Returns word that does not match here
        :param X: Strings
        :return:
        """
        str_ = X.copy().split()
        #returns = self.fit_model.gensim_model.doesnt_match(str_)
        if self.GENSIM_VERSION_4 == True:
            returns = self.fit_model.gensim_model.wv.doesnt_match(str_)
        else:
            returns = self.fit_model.gensim_model.doesnt_match(str_)
        return returns  


class pretrained_w2Vec(w2Vec):
    def __init__(self, pretrained_w2v_name="word2vec-google-news-300", names=None, column_name="Text", cache=None, progress_bar=True,
                 n_jobs=1): 
        """
        pretrained models under: https://radimrehurek.com/gensim/auto_examples/howtos/run_downloader_api.html
        """
        self.pretrained_w2v_name = pretrained_w2v_name
        self.names = names
        self.progress_bar = progress_bar
        self.n_jobs = n_jobs
        self.column_name = column_name
        self.GENSIM_VERSION_4 = check_version()

        if cache is not None:
            memory = Memory(cache, verbose=0)
            self.cache = cache
            self.transform = memory.cache(self.transform, ignore=['self'])
        else:
            self.cache = None

    def fit(self, X, y=None):
        self.fit_model = gensim.downloader.load(self.pretrained_w2v_name)
        
        if self.GENSIM_VERSION_4 == True:
            size = self.fit_model.vector_size
        else:
            size = self.fit_model.size

        if self.names == None:
            self.columns = ["Embedding_" + str(i + 1) for i in range(size)] # self.fit_model.vector_size #.size for 3.8.3
        else:
            self.columns = [self.names + str(i + 1) for i in range(size)] # #.size for 3.8.3

        return self

    def deep_transform(self, X, y=None):
        # similar transform function as in gensim_4_class
        if self.GENSIM_VERSION_4 == True:
            size = self.fit_model.vector_size
        else:
            size = self.fit_model.size
        words = [X]
        vectors = [self.fit_model[word] for word in words] #[self.fit_model.wv[word] for word in words]
        return np.reshape(np.array(vectors), (len(words), size))
    
    
    def _get_score(self, input):
        nwords = 1

        if self.GENSIM_VERSION_4 == True:
            size = self.fit_model.vector_size
        else:
            size = self.fit_model.size

        feature_vec = np.zeros(size, dtype="float32") #self.fit_model.vector_size,
        for word in input:
            try:
                predict_vec = self.deep_transform(word)[0]
                nwords = nwords + 1
                feature_vec = np.add(feature_vec, predict_vec)
            except:
                pass
        feature_vec = np.divide(feature_vec, nwords)
        return feature_vec
    
    def get_most_similar_word(self, X, topn=10):
        """
        :param X: Word that is been looked up
        :param topn: Number of words returned
        :return:
        """
        
        returns = self.fit_model.most_similar(X, topn=topn)
        return returns

    def get_most_similar_example(self, postive=None, negative=None, topn=10):
        """

        :param postive: Positive words
        :param negative: negative words
        :param topn: Top n words returned
        :return:
        """
        returns = self.fit_model.most_similar(postive, negative, topn=topn)
        return returns

    def get_word_doesnot_match(self, X):
        """
        Returns word that does not match here
        :param X: Strings
        :return:
        """
        str_ = X.copy().split()
        #returns = self.fit_model.gensim_model.doesnt_match(str_)
        returns = self.fit_model.doesnt_match(str_)
        return returns
