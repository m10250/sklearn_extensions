from sklearn.decomposition import LatentDirichletAllocation, TruncatedSVD
import pandas as pd
from sklearn.base import TransformerMixin, BaseEstimator
from joblib import Memory

class TopicModelingLDA(BaseEstimator, TransformerMixin):
    """
    LDA Topic model 

    names: Column in which the text is 

    cache: joblib cache dir
    """
    def __init__(self,model=LatentDirichletAllocation(), names=None, cache=None):
        self.model = model
        self.names = names

        if cache is not None:
            memory = Memory(cache, verbose=0)
            self.cache = cache
            self.transform = memory.cache(self.transform)
        else:
            self.cache=None

    def fit(self, X,y=None):
        X_data = X.copy()
        LDA = self.model
        fitted = LDA.fit(X_data)
        self.word_names = X.columns.to_list() # get the word names from input
        if self.names == None:
            self.columns = ["Topic_" + str(i+1) for i in range(fitted.n_components)]
        else:
            self.columns = [self.names + str(i+1) for i in range(fitted.n_components)]
        self.fitted_LDA = fitted
        return self

    def transform(self, X):
        X_data = X.copy()
        results = self.fitted_LDA.transform(X_data)
        results = pd.DataFrame(results, columns=self.get_feature_names(), index=X.index)
        return results

    def get_feature_names(self):
        return self.columns
    
    def get_feature_names_out(self):
        """get feature names of transform for sklearn compatibility > 1.0.0 """
        return self.columns

    def get_topics(self):
        topics_words = self.fitted_LDA.components_
        topics_words = pd.DataFrame(topics_words, columns= self.word_names)
        return topics_words


class TopicModelingLSI(BaseEstimator, TransformerMixin):
    """
    LSI Topic model 

    names: Column in which the text is 

    cache: joblib cache dir
    """

    def __init__(self, model=TruncatedSVD(), names=None, cache=None, **kwargs):
        self.LSI = model
        self.names = names

        if cache is not None:
            memory = Memory(cache, verbose=0)
            self.cache = cache
            self.transform = memory.cache(self.transform)
        else:
            self.cache = None

    def fit(self, X,y=None):
        """Fit the topic model on X """
        X_data = X.copy()
        LSI = self.model
        fitted = LSI.fit(X_data)
        self.word_names = X.columns.to_list() # get the word names from input
        if self.names == None:
            self.columns = ["Topic_" + str(i+1) for i in range(fitted.n_components)]
        else:
            self.columns = [self.names + str(i+1) for i in range(fitted.n_components)]
        self.fitted_LSI = fitted
        return self

    def transform(self, X):
        """Transform X to topic matrix """
        X_data = X.copy()
        results = self.fitted_LSI.transform(X_data)
        results = pd.DataFrame(results, columns=self.get_feature_names(), index=X_data.index)
        return results

    def get_feature_names(self):
        return self.columns
    
    def get_feature_names_out(self):
        """get feature names of transform for sklearn compatibility > 1.0.0 """
        return self.columns

    def get_topics(self):
        topics_words = self.fitted_LSI.components_
        topics_words = pd.DataFrame(topics_words, columns= self.word_names)
        return topics_words
