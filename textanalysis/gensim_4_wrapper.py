from gensim.models.doc2vec import TaggedDocument
import pandas as pd
from sklearn.base import TransformerMixin, BaseEstimator
import numpy as np
import gensim.downloader
from joblib import Parallel, delayed, Memory
from tqdm import tqdm
from nltk.tokenize import sent_tokenize, word_tokenize
from gensim.models.doc2vec import Doc2Vec
from gensim.models.word2vec import Word2Vec
from six import string_types

# Inspired by https://github.com/RaRe-Technologies/gensim/blob/release-3.8.3/gensim/sklearn_api/d2vmodel.py
# and https://github.com/RaRe-Technologies/gensim/blob/release-3.8.3/gensim/sklearn_api/w2vmodel.py

"""
Written sklearn wrappers similar to gensim 3.8.3 for Gensim 4
"""

class D2VTransformer(TransformerMixin, BaseEstimator):
    def __init__(self, dm=1, vector_size=100, window=5, alpha=0.025, min_alpha=0.0001, seed=1, min_count=5, max_vocab_size=None, sample=1e-3, workers=3, epochs=10,hs=0,
                 negative=5, cbow_mean=1,ns_exponent=0.75,dm_mean=None,dm_concat=0,dm_tag_count=1,dbow_words=0,
                 hashfxn=hash,sorted_vocab=1, batch_words=10000, docvecs=None, docvecs_mapfile=None, comment=None, trim_rule=None):

        self.dm = dm
        self.vector_size = vector_size
        self.window = window
        self.alpha = alpha
        self.min_alpha = min_alpha
        self.seed = seed
        self.min_count = min_count
        self.max_vocab_size = max_vocab_size
        self.sample = sample
        self.workers = workers
        self.epochs = epochs
        self.hs = hs
        self.negative = negative
        self.cbow_mean = cbow_mean
        self.ns_exponent = ns_exponent
        self.dm_mean = dm_mean
        self.dm_concat = dm_concat
        self.dm_tag_count = dm_tag_count
        self.dbow_words = dbow_words
        self.hashfxn = hashfxn
        self.sorted_vocab = sorted_vocab
        self.batch_words = batch_words
        self.docvecs = docvecs
        self.docvecs_mapfile = docvecs_mapfile
        self.comment = comment
        self.trim_rule = trim_rule

        self.gensim_model = None

    def fit(self, X, y=None):
        # Aligned to https://github.com/RaRe-Technologies/gensim/blob/release-3.8.3/gensim/sklearn_api/d2vmodel.py

        if isinstance([i for i in X[:1]][0], TaggedDocument):
            d2v_sentences = X
        else:
            d2v_sentences = [TaggedDocument(words, [i]) for i, words in enumerate(X)]

        self.gensim_model = Doc2Vec(
            documents=d2v_sentences, dm_mean=self.dm_mean, dm=self.dm,
            dbow_words=self.dbow_words, dm_concat=self.dm_concat, dm_tag_count=self.dm_tag_count,
             comment=self.comment,
            trim_rule=self.trim_rule, vector_size=self.vector_size, alpha=self.alpha, window=self.window,
            min_count=self.min_count, max_vocab_size=self.max_vocab_size, sample=self.sample,
            seed=self.seed, workers=self.workers, min_alpha=self.min_alpha, hs=self.hs,
            negative=self.negative, cbow_mean=self.cbow_mean, hashfxn=self.hashfxn,
            epochs=self.epochs, sorted_vocab=self.sorted_vocab, batch_words=self.batch_words
        ) # docvecs=self.docvecs, docvecs_mapfile=self.docvecs_mapfile
        return self

    def transform(self, X):
        if self.gensim_model != None:
            #if isinstance(X[0], string_types):
            docs = [X]
            vectors = [self.gensim_model.infer_vector(doc) for doc in docs]
            return np.reshape(np.array(vectors), (len(docs), self.gensim_model.vector_size))

class W2VTransformer(TransformerMixin, BaseEstimator):
    def __init__(self, vector_size=100, alpha=0.025, window=5, min_count=5, max_vocab_size=None, sample=1e-3, seed=1, workers=3, min_alpha=0.0001, sg=0, hs=0,
                 negative=5, cbow_mean=1, hashfxn=hash, epochs=5, null_word=0, trim_rule=None, sorted_vocab=1, batch_words=10000, ns_exponent=0.75, compute_loss=False, max_final_vocab=None):

        self.vector_size = vector_size
        self.window = window
        self.alpha = alpha
        self.min_alpha = min_alpha
        self.seed = seed
        self.min_count = min_count
        self.max_vocab_size = max_vocab_size
        self.sample = sample
        self.workers = workers
        self.epochs = epochs
        self.hs = hs
        self.negative = negative
        self.cbow_mean = cbow_mean
        self.hashfxn = hashfxn
        self.null_word = null_word
        self.sorted_vocab = sorted_vocab
        self.batch_words = batch_words
        self.trim_rule = trim_rule
        self.ns_exponent = ns_exponent
        self.compute_loss = compute_loss
        self.sg = sg
        self.max_final_vocab = max_final_vocab





        self.gensim_model = None

    def fit(self, X, y=None):
        self.gensim_model = Word2Vec(
            sentences=X, vector_size=self.vector_size, alpha=self.alpha,
            window=self.window, min_count=self.min_count, max_vocab_size=self.max_vocab_size,
            sample=self.sample, seed=self.seed, workers=self.workers, min_alpha=self.min_alpha,
            sg=self.sg, hs=self.hs, negative=self.negative, cbow_mean=self.cbow_mean,
            hashfxn=self.hashfxn, epochs=self.epochs, null_word=self.null_word, trim_rule=self.trim_rule,
            sorted_vocab=self.sorted_vocab, batch_words=self.batch_words, max_final_vocab=self.max_final_vocab,
            compute_loss=self.compute_loss,ns_exponent=self.ns_exponent
        )

        
        return self

    def transform(self, X):
        if self.gensim_model != None:
            # The input as array of array
            if isinstance(X, string_types):
                words = [X]
                vectors = [self.gensim_model.wv[word] for word in words]
                return np.reshape(np.array(vectors), (len(words), self.vector_size))
            
