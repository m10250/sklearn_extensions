import pandas as pd
from nltk.tokenize import word_tokenize, sent_tokenize
from nltk.stem.porter import PorterStemmer
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import stopwords
from sklearn.base import TransformerMixin, BaseEstimator
import pickle
from joblib import Parallel, delayed, Memory
from tqdm import tqdm
import copy

#lemmatizer = WordNetLemmatizer()

def preprocess_words(text, language="english",lower_case=True, position=None):
    """
    Basic preprocessing pipline
    :param text: Input text
    :param language: chosen language
    :param lower_case: use lower case
    :return: preprocess words in text
    """
    words = word_tokenize(text, language)
    stopwords_list = set(stopwords.words(language))
    filtered_words = [w for w in words if not w.lower() in stopwords_list]
    filtered_words = [w for w in filtered_words if w.isalpha()]
    if lower_case == True:
        words = [w.lower() for w in filtered_words]
    else:
        words = filtered_words
    return words

def preprocess_words_stemming(text, language="english", lower_case=True, position=None):
    """
    use stemming on text with preprocessing
    :param text:
    :param language:
    :param lower_case:
    :param kwargs: for stemmer
    :return:
    """
    words = preprocess_words(text, language, lower_case)
    ps = PorterStemmer()
    stemmed = [ps.stem(w) for w in words]

    return stemmed


def preprocess_words_lemma(text, language="english", lower_case=True, position=["n"]):
    """
    Lemmatizer
    :param text:
    :param language:
    :param lower_case:
    :param kwargs: for WordNetLemmatizer
    :return:
    """
    words =preprocess_words(text, language, lower_case)
    # Can cause issues with threading see: https://stackoverflow.com/questions/27433370/what-would-cause-wordnetcorpusreader-to-have-no-attribute-lazycorpusloader
    lemmatizer = copy.deepcopy(WordNetLemmatizer())
    lemmas = words
    for posi in position:
        lemmas = [lemmatizer.lemmatize(w, pos=posi) for w in lemmas]

    return lemmas

class textpreprocessing(BaseEstimator, TransformerMixin):
    def __init__(self, function, language="english", lower_case=True, position=["n"], path=None, save_path=None, fileformat=".txt", cache=None, n_jobs=1, progress_bar=True, name="Text"):
        """
        Wrapper for preprocessing functions
        If path is specified, it reads files from disk
        If save_path is specified, it writes process files to disk in pkl format
        :param function: Preprocessing function
        :param language:
        :param lower_case:
        :param position: for Lemmatizer -> list of positions
        :param path: read path -> if non from memory
        :param save_path: save each preprocessed file to disk
        :param name: Column name for pandas data frame to store
        """
        self.language=language
        self.lower_case=lower_case
        self.function = function
        self.position = position
        self.columns = "Texts"
        self.path = path
        self.save_path = save_path
        self.fileformat = fileformat
        self.progress_bar = progress_bar
        self.n_jobs = n_jobs
        self.name = name

        if cache is not None:
            memory = Memory(cache, verbose=0)
            self.cache = cache
            self.transform=memory.cache(self.transform)
        else:
            self.cache = cache


    def fit(self, X, y=None):
        return self

    def transform(self, X):
        # TODO: Multiprocessing
        def preprocess(text, index_):
            return_values=[]
            if self.path != None: # read from Disk
                    file_name = self.path + text
                    file = open(file_name, "r") # open file
                    if self.function == None:
                        return_values.append(file) # append file pointer
                    else: # if preprocess function -> apply function
                        preprocessed_texts = self.function(file.read(), self.language, self.lower_case,self.position)
                        if self.save_path != None: # if results should not be saved to disk, keep them im RAM
                            return_values.append(' '.join(preprocessed_texts)) # preprocessed_texts
                        else:
                            # save file by file to disk -> lowest RAM usage
                            save_file_name = self.save_path + index_ + self.fileformat
                            with open(save_file_name) as f:
                                f.write(' '.join(preprocessed_texts)) # write as text
                                f.close()
                            return_values.append(save_file_name)
            else: # if not read from disk, read from memory
                if self.function != None:
                    if self.save_path == None: # apply function and keep in RAM if no save path
                        preprocessed_texts = self.function(text, self.language, self.lower_case, self.position)
                        return_values = ' '.join(preprocessed_texts) #return text as one string #self.function(text, self.language, self.lower, self.position)
                    else:
                        # process file by file and write to disk for low RAM usage
                            file_name = self.path + text
                            file = open(file_name, "r")
                            preprocessed_texts = self.function(file.read(), self.language, self.lower_case, self.position)
                            save_file_name = self.save_path + index_ + self.fileformat
                            with open(save_file_name) as f:
                                f.write(' '.join(preprocessed_texts)) # write as text
                                f.close()
                            return_values.append(save_file_name)
                else:
                    # just return the file names
                    return_values = self.path + text

            return return_values

        if self.progress_bar == True:
            r = Parallel(n_jobs=self.n_jobs)(delayed(preprocess)(i, i.index) for i in tqdm(X, desc="Preprocessing Texts"))
        else:
            r = Parallel(n_jobs=self.n_jobs)(delayed(preprocess)(i, i.index) for i in X)
        #print(r)
        r = pd.DataFrame(r, index=X.index, columns=[self.name])
        return r

    def get_feature_names(self):
        return self.columns
    
    def get_feature_names_out(self):
        """get feature names of transform for sklearn compatibility > 1.0.0 """
        return self.columns
