from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
import pandas as pd
from sklearn.base import TransformerMixin, BaseEstimator
from joblib import Memory

class TransformToDense(BaseEstimator, TransformerMixin):
    """
    Transform result array to dense format
    column_names: Selected columns
    """
    def __init__(self, column_names=None):
        super().__init__()
        self.column_names = column_names

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        if self.column_names == None:
            return X.todense()
        else:
            data = pd.DataFrame(X.todense(), columns=self.column_names)
            return data


class TFIDF(BaseEstimator, TransformerMixin):
    """
    TF-IDF Transformer based on Sklearn

    columnname: Column in which the text is 

    cache: joblib cache dir
    """
    def __init__(self, model=TfidfVectorizer(), columnname="Text", cache=None, **kwargs):
        self.model = model
        self.columnname = columnname

        if cache is not None:
            memory = Memory(cache, verbose=0)
            self.cache = cache
            self.transform = memory.cache(self.transform)
        else:
            self.cache = None

    def fit(self, X,y=None):
        X_data = X[self.columnname].copy()
        tfidf = self.model
        fitted = tfidf.fit(X_data)
        self.columns = fitted.get_feature_names_out()
        self.fitted_tfidf = fitted
        return self

    def transform(self, X):
        X_data = X[self.columnname].copy()
        results = self.fitted_tfidf.transform(X_data)
        #print(results.todense())
        results = pd.DataFrame(results.todense(), columns=self.get_feature_names(), index=X.index)
        return results

    def get_feature_names(self):
        return self.columns
    
    def get_feature_names_out(self):
        """get feature names of transform for sklearn compatibility > 1.0.0 """
        return self.columns


class BagOfWords(BaseEstimator, TransformerMixin):
    """
    BoW based on sklearn

    columnname: Column in which the text is 

    cache: joblib cache dir

    """
    def __init__(self,model=CountVectorizer(), columnname="Text", cache=None ,**kwargs):
        self.columnname=columnname
        self.model = model

        if cache is not None:
            memory = Memory(cache, verbose=0)
            self.cache = cache
            self.transform = memory.cache(self.transform)
        else:
            self.cache = None

    def fit(self, X,y=None):
        X_data = X[self.columnname].copy()
        bagofwords = self.model
        fitted = bagofwords.fit(X_data)
        self.columns = fitted.get_feature_names_out()
        self.fitted_bagofwords = fitted
        return self

    def transform(self, X):
        X_data = X[self.columnname].copy()
        results = self.fitted_bagofwords.transform(X_data)
        results = pd.DataFrame(results.todense(), columns=self.get_feature_names(), index=X.index)
        return results

    def get_feature_names(self):
        return self.columns
    
    def get_feature_names_out(self):
        """get feature names of transform for sklearn compatibility > 1.0.0 """
        return self.columns
