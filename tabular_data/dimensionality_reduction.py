import pandas as pd
from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.base import BaseEstimator, TransformerMixin


class PrincipalComponentAnalysis(BaseEstimator, TransformerMixin):
    """
    PCA transforming features; based on PCA of Sklearn
    Returning dataframe with names for the name argument
    Columns = list of columns to apply on
    """

    def __init__(self,model=PCA(), columns=None,col_names=None, **kwargs):
        self.model = model
        self.columns = columns
        self.col_names = col_names
        self.n_components = model.n_components

    def fit(self, X,y=None):
        X_data = X.copy()
        if self.columns is not None:
           X_data=X_data[self.columns].copy()

        self.pca_fit = self.model.fit(X_data,y)

        if self.col_names is not None:
            self.columnnames = ["PCA_" + str(self.col_names)+"_"+ str(x) for x in range(self.n_components)]
        else:
            self.columnnames = ["PCA_" + str(x) for x in range(self.n_components)]

        return self

    def transform(self, X, y=None):
        X_data = X.copy()
        if self.columns is not None:
            X_data = X_data[self.columns].copy()
        transformer = self.pca_fit
        transformed_data = transformer.transform(X_data)
        self.n_components = len(transformer.components_)

        if self.col_names is not None:
            self.columnnames = ["PCA_" + str(self.col_names)+"_"+ str(x) for x in range(self.n_components)]
        else:
            self.columnnames = ["PCA_" + str(x) for x in range(self.n_components)]

        # build output
        if self.columns is not None:
            transformed_data = pd.DataFrame(transformed_data, columns=self.columnnames, index=X_data.index)
            X_data = X.copy()
            X_data.drop(self.columns, axis=1, inplace=True)
            X_data.loc[:,transformed_data.columns]=transformed_data
            transformed_data=X_data.copy()
        else:
            transformed_data=pd.DataFrame(transformed_data, columns=self.columnnames, index=X_data.index)
        return transformed_data

    def explained_variance_ration(self):
        return self.pca.explained_variance_ratio_

    def components_(self):
        return self.pca.components_

    def get_feature_names(self):
        return self.columnnames
    
    def get_feature_names_out(self):
        """get feature names of transform for sklearn compatibility > 1.0.0 """
        return self.columnnames


class LDA_Analysis(BaseEstimator, TransformerMixin):
    """
    LDA (Linear Discriminat Analysis) transforming features; based on LDA of Sklearn
    Returung dataframe with names for the name argument
    Columns = list of columns to apply on
    """

    def __init__(self,model=LinearDiscriminantAnalysis(), columns=None,name=None, **kwargs):
        self.model = model
        self.columns = columns
        self.name = name

    def fit(self, X,y=None):
        X_data = X.copy()
        if self.columns is not None:
           X_data=X_data[self.columns].copy()

        self.pca_fit = self.model.fit(X_data,y)
        return self

    def transform(self, X):
        X_data = X.copy()
        if self.columns is not None:
            X_data = X_data[self.columns].copy()
        transformer = self.pca_fit
        transformed_data = transformer.transform(X_data)
        self.n_components = len(transformer.components_)

        if self.name is not None:
            self.columnnames = ["PCA_" + str(self.name)+"_"+ str(x) for x in range(self.n_components)]
        else:
            self.columnnames = ["PCA_" + str(x) for x in range(self.n_components)]

        # build output
        if self.columns is not None:
            transformed_data = pd.DataFrame(transformed_data, columns=self.columnnames, index=X_data.index)
            X_data = X.copy()
            X_data.drop(self.columns, axis=1, inplace=True)
            X_data.loc[:,transformed_data.columns]=transformed_data
            transformed_data=X_data.copy()
        else:
            transformed_data=pd.DataFrame(transformed_data, columns=self.columnnames, index=X_data.index)
        return transformed_data

    def explained_variance_ration(self):
        return self.pca.explained_variance_ratio_

    def components_(self):
        return self.pca.coef_

    def get_feature_names(self):
        return self.columnnames
