from joblib import Parallel, delayed, Memory
import pandas as pd
from sklearn.base import RegressorMixin, BaseEstimator, ClassifierMixin
import numpy as np
from tensorflow.keras.models import Model, load_model
from tensorflow.keras import backend as K
import tensorflow as tf
from sklearn.model_selection import train_test_split
import gc
import os


# Cores Pipeline
# cores training

# TODO set input shape

# When sklearn-intelx is used:
# This TensorFlow binary is optimized with oneAPI Deep Neural Network Library (oneDNN)

class wrapperKeras(BaseEstimator):
    def __init__(self, model_function, datapipeline_function=None, batch_size=32, epochs=20,
                 optimizer="adam", loss_function="mae", metrics=None, n_jobs=None, model_params=None, pipeline_params=None, shuffle_seed=42, shuffle_size=5000, validation_size=None, prefetch=5, load_model=None, custom_objects=None, callbacks=None): #**params

        self.model_function = model_function
        self.datapipeline_function = datapipeline_function
        #self.img_col_name = img_col_name
        #self.img_path = img_path
        self.pipeline_params = pipeline_params
        self.batch_size = batch_size
        self.epochs= epochs
        self.optimizer = optimizer
        self.loss_function = loss_function
        self.metrics = metrics
        self.n_jobs = n_jobs
        self.model_params = model_params
        self.prefetch = prefetch
        self.shuffle_seed = shuffle_seed
        self.shuffle_size = shuffle_size
        self.validation_size = validation_size
        self.load_model = load_model
        self.custom_objects = custom_objects
        self.callbacks = callbacks

        # https://github.com/tensorflow/tensorflow/issues/29968
        #if self.n_jobs == None: # create GPU Session:
        #    self.sess = tf.device('/GPU:0')
        #else:
        #    self.sess = tf.device('/CPU:0')
        #    tf.config.threading.set_inter_op_parallelism_threads(self.n_jobs)

    def fit(self, X,y):
        # Create TF Session
        # https://github.com/tensorflow/tensorflow/issues/29968
        if self.n_jobs == None:  # create GPU Session:
            self.sess = tf.device('/GPU:0')
        else:
            tf.config.threading.set_inter_op_parallelism_threads(self.n_jobs)
            self.sess = tf.device('/CPU:0')
            # os.environ["OMP_NUM_THREADS"] = "1" #?



        # Check if load should only be loaded

        if self.load_model != None:
            self.model = load_model(self.load_model, custom_objects=self.custom_objects)
            return self

        else: # if real training
            # store y size
            self.y_size = y.shape
            try:
                self.y_size[1] # if only vector
            except:
                self.y_size=(self.y_size[0], 1)

            # create pipeline

            # if datapipeline
            if self.datapipeline_function != None:

                # Train-Val-Split if chosen
                if self.validation_size != None:
                    X_train, X_val, y_train, y_val = train_test_split(X,y, test_size=self.validation_size, seed=self.shuffle_seed, stratify=y)
                else:
                    X_train = X.copy()
                    y_train = y.copy()

                # Create pipelines with and without parameters
                if self.pipeline_params != None:
                    datapipeline = self.datapipeline_function(X_train,y_train,self.pipeline_params)
                    if self.validation_size != None:
                        validation_pipeline = self.self.datapipeline_function(X_val,y_val,self.pipeline_params)
                        validation_pipeline = validation_pipeline.batch(
                            self.batch_size).prefetch(self.prefetch)
                else:
                    datapipeline = self.datapipeline_function(X_train,y_train)
                    if self.validation_size != None:
                        validation_pipeline = self.self.datapipeline_function(X_val,y_val)
                        validation_pipeline = validation_pipeline.batch(
                            self.batch_size).prefetch(self.prefetch)


                datapipeline = datapipeline.shuffle(self.shuffle_size, seed=self.shuffle_seed).batch(self.batch_size).prefetch(self.prefetch)


                #Extract Shape X
                it = iter(datapipeline)
                if type(next(it)[0]) == tuple: # extract X from it; check if tuple
                    shape_list = [] # for each part of x extract the dimensionality
                    for i in range(len(next(it)[0])):
                        temp = next(it)[0][i].numpy().shape
                        if len(temp) > 2: # if multi-dimensional shape exclude batch
                            shape_list.append(temp[1:])
                        else:
                            shape_list.append(temp[1])
                    self.input_shape = shape_list
                else:
                    # single input case
                    sample = next(it)[0].numpy() # get x of iterator
                    shape = sample.shape
                    if len(shape)==2:
                        self.input_shape = shape[1] # exclude batch dimension
                    else:
                        self.input_shape = shape[1:] # image, exclude batch

                # create model
                if self.model_params != None:
                    model = self.model_function(self.input_shape, self.model_params)
                else:
                    model = self.model_function(self.input_shape)

                model.compile(loss=self.loss_function, metrics=self.metrics, optimizer=self.optimizer)
                print(model.summary())

                model.compile(loss=self.loss_function, metrics=self.metrics, optimizer=self.optimizer)
                print(model.summary())

                # Create Training Session + Train
                with self.sess:
                    if self.validation_size !=None:
                        model.fit(datapipeline, validation_data = validation_pipeline, epochs=self.epochs, callbacks=self.callbacks)
                        self.model = model
                    else:
                        model.fit(datapipeline, epochs=self.epochs, callbacks=self.callbacks)
                        self.model = model
            else:
                # TODO extract shape of X for building model
                if len(X.shape)>2: # image shape
                    self.input_shape = X.shape[1:]
                else:
                    self.input_shape = X.shape[1]

                # create model
                if self.model_params != None:
                    model = self.model_function(self.input_shape, self.model_params)
                else:
                    model = self.model_function(self.input_shape)

                model.compile(loss=self.loss_function, metrics=self.metrics, optimizer=self.optimizer)
                print(model.summary())

                # Create Trainingsession without pipeline and train
                with self.sess:
                    if self.validation_size != None:
                        model.fit(X,y, epochs=self.epochs, batch_size=self.batch_size, validation_split=self.validation_size, callbacks=self.callbacks)#
                        self.model = model
                    else:
                        model.fit(X, y, epochs=self.epochs, batch_size=self.batch_size,callbacks=self.callbacks)
                        self.model = model
            K.clear_session()
            gc.collect()

        return self

    def predict(self, X, y=None):

        # https://github.com/tensorflow/tensorflow/issues/29968
        if self.n_jobs == None:  # create GPU Session:
            self.sess = tf.device('/GPU:0')
        else:
            tf.config.threading.set_inter_op_parallelism_threads(self.n_jobs)
            self.sess = tf.device('/CPU:0')
            # os.environ["OMP_NUM_THREADS"] = "1" #?


        model = self.model
        y = np.zeros((len(X), self.y_size[1])) # create fake y to fit in pipeline

        if self.datapipeline_function != None:
            if self.pipeline_params != None:
                datapipeline = self.datapipeline_function(X,y,self.pipeline_params)
            else:
                datapipeline = self.datapipeline_function(X,y)
            datapipeline = datapipeline.batch(
                    self.batch_size).prefetch(self.prefetch)
            with self.sess:
                prediction = model.predict(datapipeline)
        else:
            with self.sess:
                prediction = model.predict(X)

        return prediction

class wrapperKerasRegressor(wrapperKeras, RegressorMixin):
    #def __init__(self):
    #    super().__init__()

    def empty_class(self):
        return self


class wrapperKerasClassifier(wrapperKeras, ClassifierMixin):
    #def __init__(self):
    #    super().__init__()

    def predict_proba(self, X, y=None):
        return self.predict(X,y)





