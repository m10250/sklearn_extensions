import pandas as pd
from sklearn.base import BaseEstimator, RegressorMixin
import statsmodels.api as sm
#from statsmodels.tools import * # import add_constant function
from sklearn.metrics import r2_score
import numpy as np
from scipy.stats import ttest_1samp

# https://stackoverflow.com/questions/24851787/statsmodels-calculate-fitted-values-and-r-squared/24852415#24852415
# https://stats.stackexchange.com/questions/259820/r2-score-scikit-learn-vs-statsmodels

class statsmodels_regression(BaseEstimator, RegressorMixin):
    """
    Wrapper for the statsmodel regression

    If add_constant is set, a constant to the data is added

    The function model_weights retrieves the model weights in a summary output

    The function _get_params gets the parameters

    """

    def __init__(self,add_constant=False):
        self.add_constant=add_constant

    def fit(self, X,y):
        X_data=X.copy()
        if self.add_constant==True:
            X_data["Intercept"]=1
            #X_data = sm.add_constant(X_data, has_constant='add')
        regresssion = sm.OLS(y,X_data, hasconst=self.add_constant) #
        fit_regression = regresssion.fit()

        self.regression_model = fit_regression
        self.n = len(X_data.columns.to_list())
        #print(self.n)
        return self

    def predict(self, X, y=None):
        X_data = X.copy()
        if (self.add_constant==True) and (len(X_data.columns.to_list()) < self.n):
            X_data["Intercept"]=1
            #X_data = sm.add_constant(X_data, has_constant='add')
        prediction = self.regression_model.predict(X_data)
        return prediction

    def model_weights(self, float_format=None):
        if float_format is not None:
            return self.regression_model.summary2(float_format=float_format)
        else:
            return self.regression_model.summary()

    def _get_params(self):
        return self.regression_model.params

    def get_feature_names(self):
        return self.regression_model.params.index.to_list()
    
    def get_feature_names_out(self):
        """get feature names of transform for sklearn compatibility > 1.0.0 """
        return self.column_names.to_list()
    
    def coef_(self):
        return self.regression_model.params
    
    def check_linear_assumptions(self, X,y):
        """
        returns dictionary with the results of the assumptions
        checks on conditional mean, multi_collinearity, heteroscedasticity and normal distribution of error term
        """
        # function for checking if linear assumption hold
        results = {}
        
        prediction = self.predict(X)
        error = y-prediction
        
        # https://www.albert.io/blog/key-assumptions-of-ols-econometrics-review/
        # Check assumption No.3 
        
        conditional_mean = np.mean(error)
        t_test_results = ttest_1samp(error, popmean=0.0) # two-sided test
        results["conditional_mean"] = (conditional_mean, t_test_results)
        
        # Check assumption No. 4 multi-collinearity
        multi_collinearity_check = X.corr()
        results["multi_collinearity_check"] = multi_collinearity_check
        
        # Check assumption No. 5 No homoscedasticity = heteroscedasticity
        from matplotlib import pyplot as plt
        plot = plt.scatter(prediction, error)
        plt.xlabel("prediction")
        plt.ylabel("error term")
        results["heteroscedasticity_check"] = plot
        plt.show()
        
        
        # Check normal distribution of error term
        import seaborn as sns
        plot2 = sns.distplot(error)
        results["distribution_check"] = plot2
        plt.show()
        
        return results


class statsmodels_logit_regression(BaseEstimator, RegressorMixin):
    """
    Wrapper for the statsmodel logistic regression

    If add_constant is set, a constant to the data is added

    The function model_weights retrieves the model weights in a summary output

    The function _get_params gets the parameters

    """

    def __init__(self,add_constant=False):
        self.add_constant=add_constant

    def fit(self, X,y):
        X_data=X.copy()
        if self.add_constant==True:
            X_data["Intercept"]=1
            #X_data = sm.add_constant(X_data, has_constant='add')
        regresssion = sm.Logit(y,X_data, hasconst=self.add_constant) #
        fit_regression = regresssion.fit()

        self.regression_model = fit_regression
        self.n = len(X_data.columns.to_list())
        #print(self.n)
        return self

    def predict(self, X, y=None):
        X_data = X.copy()
        if (self.add_constant==True) and (len(X_data.columns.to_list()) < self.n):
            X_data["Intercept"]=1
            #X_data = sm.add_constant(X_data, has_constant='add')
        prediction = self.regression_model.predict(X_data)
        return prediction

    def model_weights(self, float_format=None):
        if float_format is not None:
            return self.regression_model.summary2(float_format=float_format)
        else:
            return self.regression_model.summary()

    def _get_params(self):
        return self.regression_model.params

    def get_feature_names(self):
        return self.regression_model.params.index.to_list()
    
    def get_feature_names_out(self):
        """get feature names of transform for sklearn compatibility > 1.0.0 """
        return self.column_names.to_list()
    
    def coef_(self):
        return self.regression_model.params

