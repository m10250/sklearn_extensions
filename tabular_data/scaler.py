import pandas as pd
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.base import BaseEstimator, TransformerMixin


class MinimumMaximumScaler(BaseEstimator, TransformerMixin):
    """
    MinMaxScaler transforming features
    Returung dataframe with names
    Columns = list of columns to apply on
    """

    def __init__(self, scaler=MinMaxScaler(), columns=None, **kwargs):
        self.scaler = scaler
        self.columns = columns

    def fit(self, X,y=None):
        X_data = X.copy()
        if self.columns is not None:
           X_data=X_data[self.columns].copy()
        self.column_names = X_data.columns.to_list()
        
        self.scaler_fit = self.scaler.fit(X_data,y)
        return self

    def transform(self, X, y=None):
        X_data = X.copy()
        if self.columns is not None:
            X_data = X_data[self.columns].copy()
        transformer = self.scaler_fit
        transformed_data = transformer.transform(X_data)

        # build output
        if self.columns is not None:
            transformed_data = pd.DataFrame(transformed_data, columns=self.columns, index=X_data.index)
            X_data = X.copy()
            X_data.drop(self.columns, axis=1, inplace=True)
            X_data.loc[:,transformed_data.columns]=transformed_data
            transformed_data=X_data.copy()
        else:
            transformed_data=pd.DataFrame(transformed_data, columns=X.columns, index=X_data.index)
        return transformed_data
    
    def get_feature_names(self):
        return self.column_names.to_list()
    
    def get_feature_names_out(self):
        """get feature names of transform for sklearn compatibility > 1.0.0 """
        return self.column_names.to_list()


class zScaler(BaseEstimator, TransformerMixin):
    """
    StandardScaler transforming features
    Returung dataframe with names
    Columns = list of columns to apply on
    """

    def __init__(self,scaler=StandardScaler(), columns=None, **kwargs):
        self.scaler = scaler
        self.columns = columns

    def fit(self, X,y=None):
        X_data = X.copy()
        if self.columns is not None:
           X_data=X_data[self.columns].copy()

        self.scaler_fit = self.scaler.fit(X_data,y)
        self.column_names = X.columns
        return self

    def transform(self, X):
        X_data = X.copy()
        if self.columns is not None:
            X_data = X_data[self.columns].copy()
        transformer = self.scaler_fit
        transformed_data = transformer.transform(X_data)

        # build output
        if self.columns is not None:
            transformed_data = pd.DataFrame(transformed_data, columns=self.columns, index=X_data.index)
            X_data = X.copy()
            X_data.drop(self.columns, axis=1, inplace=True)
            X_data.loc[:,transformed_data.columns]=transformed_data
            transformed_data=X_data.copy()
        else:
            transformed_data=pd.DataFrame(transformed_data, columns=X.columns, index=X_data.index)
        return transformed_data

    def fit_transform(self, X, y=None, **fit_params):
        return super().fit_transform(X, y)

    def get_feature_names(self):
        return self.column_names.to_list()
    
    def get_feature_names_out(self):
        """get feature names of transform for sklearn compatibility > 1.0.0 """
        return self.column_names.to_list()
