import pandas as pd
from statsmodels.stats.outliers_influence import variance_inflation_factor
from sklearn.base import BaseEstimator, TransformerMixin

class reduceVIF(BaseEstimator, TransformerMixin):
    """
    Transformer to reduce multi-colinearity by reducing the VIF

    Threshold: Parameter when to consider to drop
    drops: How many parameters are dropped per round
    rounds: How often should the VIF be repeated; If None, just one time


    """

    def __init__(self, threshold=5, rounds=None, drops=2):
        self.threshold=threshold
        self.rounds = rounds
        self.drops = drops

    def fit(self, X,y=None):
        X_data = X.copy()
        vif = pd.DataFrame()

        if self.rounds is None: # If round not defined
            vif["VIF Factor"] = [variance_inflation_factor(X_data.values,i) for i in range(X_data.shape[1])] # calculate VIF
            vif["features"] = X_data.columns
            drop_list = vif[vif["VIF Factor"]>=self.threshold].copy() # Create a drop_list with feature to remove
            self.vif = vif # Store VIF
            self.drop_list = drop_list # Store Droplist
            drop_list.apply(lambda x: print("Dropping variable " + str(x["features"] + " with VIF " + str(x["VIF Factor"]))), axis=1)
            drop_elements = drop_list["features"]
            X_data.drop(drop_elements, axis=1, inplace=True)
            # Print which variables are droppled
        else:
            r=[]
            if self.drops > len(X_data.columns):
                print("Adjusting drops to " + str(len(X_data.columns)-2))
                self.drops = len(X_data.columns)-2 # at least 2 Columns are staying when more wished to dropped
            for round in range(self.rounds): # for each round
                vif = pd.DataFrame()
                vif["VIF Factor"] = [variance_inflation_factor(X_data.values, i) for i in range(X_data.shape[1])] # calculate VIF
                vif["features"] = X_data.columns
                drop_list = vif[vif["VIF Factor"] >= self.threshold].copy()
                drop_list.sort_values("VIF Factor", ascending=False) # Create droplist
                self.vif = vif
                drop_list = drop_list.iloc[:self.drops]
                drop_list.apply(
                    lambda x: print("Dropping variable in round " + str(round) + " " + str(x["features"] + " with VIF " + str(x["VIF Factor"]))),
                    axis=1) # Print output
                drop_elements = drop_list["features"]
                X_data.drop(drop_elements, axis=1, inplace=True) # drop elements for this round
                r.append(drop_list)
            self.drop_list=r
        self.feature_names=X_data.columns.to_list()
        return self

    def transform(self, X):
        X_data = X.copy()
        if type(self.drop_list) is list:
            drop_elements = pd.concat(self.drop_list)
            drop_elements=drop_elements["features"]
        else:
            drop_elements = self.drop_list["features"]
        X_data.drop(drop_elements, axis=1, inplace=True) # drop elements from fit
        return X_data

    def get_feature_names(self):
        return self.feature_names
    
    def get_feature_names_out(self):
        """get feature names of transform for sklearn compatibility > 1.0.0 """
        return self.feature_names
