from sklearn.base import BaseEstimator, RegressorMixin

class transformed_target(BaseEstimator, RegressorMixin):
    """
    Target trasnformer 
    regression_model = final model to be fitted to the data
    func: transform function, e.g. np.log1p
    inverse_func: inverse function, e.g. np.expm1
    """
    def __init__(self, regression_model=None, func=None, inverse_func=None):
        self.regression_model = regression_model
        self.func = func
        self.inverse_func = inverse_func

    def fit(self, X, y):
        y_transformed = self.func(y)

        self.regressor_ = self.regression_model.fit(X, y_transformed)

        return self

    def predict(self, X):
        prediction = self.regressor_.predict(X)
        retransformed_prediction = self.inverse_func(prediction)
        return retransformed_prediction
