import pandas as pd
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import PolynomialFeatures

class createPolynominals(BaseEstimator, TransformerMixin):
    """
    Function to create polynominal configuration
    columns = list of column names; where to applied on

    """

    def __init__(self, polynominal=PolynomialFeatures(), columns=None, **kwargs):
        self.polynominal = polynominal
        self.columns=columns

    def fit(self, X, y=None):
        X_data = X.copy()
        if self.columns is not None:
            X_data = X_data[self.columns].copy()
        poly = self.polynominal
        poly.fit(X_data)
        self.poly_fit=poly
        temp = [str(x) for x in X_data.columns.to_list()]
        self.feature_names = [str(x) for x in self.poly_fit.get_feature_names(temp)]
        return self

    def transform(self, X):
        X_data = X.copy()
        if self.columns is not None:
            X_data = X_data[self.columns].copy()
            names = X_data.columns.to_list()
        transformed = self.poly_fit.transform(X_data)
        transformed = pd.DataFrame(transformed, columns=self.feature_names, index=X_data.index)

        if self.columns is not None:
            X_data=X.copy()
            X_data.drop(self.columns, axis=1, inplace=True)
            X_data.loc[:, self.feature_names]=transformed
            return_value = X_data
        else:
            return_value = pd.DataFrame(transformed, columns=self.feature_names, index=X_data.index)
        return return_value

    def get_feature_names(self):
        return self.feature_names
    
    def get_feature_names_out(self):
        """get feature names of transform for sklearn compatibility > 1.0.0 """
        return self.column_names
