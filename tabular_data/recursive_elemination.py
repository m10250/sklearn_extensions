from sklearn.base import BaseEstimator
from sklearn.base import MetaEstimatorMixin
from sklearn.feature_selection import SelectorMixin
from sklearn.feature_selection import RFE
import numpy as np
import pandas as pd

# SelectorMixin
class feature_selector(MetaEstimatorMixin, BaseEstimator):
    """
    Performs feature selection
    
    rfe_estimator: model to fit
    proportion_of_features: percentage of how many variables that are kept, float (0,1)
    steps: how many variables are deleted at once, int
    methods: which method to apply - feature importance or coeff
    transform: use squared transformation of coeffs to just delete small coeff, however do not only delete negative ones
    verbose: print delete variables
    """
    def __init__(self, rfe_estimator, proportion_of_features=0.5, steps=1, method="coef", verbose=False, transform_coeff=True, backend="sklearn"): # transform=True
        self.rfe_estimator = rfe_estimator
        self.proportion_of_features = proportion_of_features
        self.steps = steps
        self.method = method
        self.verbose = verbose
        self.transform_coeff = transform_coeff
        self.backend = backend
    
    def fit(self,X,y):
        X_copy = X.copy()
        number_of_columns = len(X_copy.columns.to_list())
        number_of_columns_to_select = np.floor(number_of_columns * self.proportion_of_features)
        
        while number_of_columns > number_of_columns_to_select:
            self.estimator_ = self.rfe_estimator.fit(X_copy,y)
            
            if self.method == "coef":
                if self.backend == "sklearn":
                    params = self.estimator_.coef_ #coef_()
                    params = pd.Series(params, index=X_copy.columns.to_list()) # transform numpy array to series
                else:
                    params = self.estimator_.coef_()
                if self.transform == True:
                    params = params**2
            else:
                # TODO test
                feature_importance = self.estimator_.feature_importances_
                params = pd.Series(feature_importance, index=X_copy.columns.to_list())
            
            params.sort_values(ascending=False, inplace=True)
            
            # deselect the worst columns
            parameters_to_drop = params.tail(self.steps).index.to_list()
            set_to_drop = set(parameters_to_drop).intersection(X_copy.columns.to_list()) # to exclude Intercept
            if self.verbose == True:
                print("Dropping variables ")
                print(set_to_drop) # parameters to drop
                
            X_copy.drop(set_to_drop, axis=1, inplace=True)
            
            """params.drop(params.tail(self.steps).index, inplace=True)
            column_names_to_keep = list(params.index.to_list())
            X_copy = X_copy[column_names_to_keep].copy()
            """
            
            """params_selected = params.iloc[:-self.steps]
            print(params.iloc[-self.steps:])
            column_names_to_keep = params_selected.index.to_list()
            print(column_names_to_keep)
            
            X_copy = X_copy[column_names_to_keep].copy()"""
            
            number_of_columns = len(X_copy.columns.to_list())
            self.keep_columns = X_copy.columns.to_list() #column_names_to_keep
        
        self.estimator_ = self.rfe_estimator.fit(X_copy,y) # refit 
        
        # Debug here when estimator has no attribute get_feature_names
        if hasattr(self.estimator_, "get_feature_names"):
            self.column_names = self.estimator_.get_feature_names()
        else:
            self.column_names = list(self.estimator_.feature_names_in_)
        return self
    
    def transform(self, X,y=None):
        X_return=X[self.keep_columns].copy()
        return X_return
    
    def predict(self, X):
        X_data = self.transform(X)
        prediction = self.estimator_.predict(X_data)
        return prediction
    
    def get_feature_names(self):
        return self.column_names.to_list()
    
    def get_feature_names_out(self):
        """get feature names of transform for sklearn compatibility > 1.0.0 """
        return self.column_names
