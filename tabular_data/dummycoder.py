import pandas as pd
from sklearn.preprocessing import OneHotEncoder
from sklearn.base import BaseEstimator, TransformerMixin
import numpy as np
import warnings


class Dummyencoder(BaseEstimator, TransformerMixin):
    """Create Dummy Coding with n-1 dummies for the selected columns

    columns: selected columns which should be transformed to dummies
    transformer: Sklearn OneHotEncoder
    drop_first: deprecated -> included into transformer

    """

    def __init__(self,columns, transformer = OneHotEncoder(drop="first", sparse=False, handle_unknown='ignore'), drop_first=True):
        self.categories = None
        self.drop_first = drop_first
        self.columns=columns
        self.transformer = transformer

    def fit(self, X, y=None):
        X_selected = X.loc[:, self.columns].copy()
        self.fit_ = self.transformer.fit(X_selected, y)
        self.column_names = self.fit_.get_feature_names_out()
        return self

    def transform(self, X):
        warnings.filterwarnings("ignore", category=UserWarning) #ignore message that Found unknown categories […] in column
        X_selected = X.loc[:, self.columns].copy()
        X_transformed = self.fit_.transform(X_selected)
        X_transformed = pd.DataFrame(X_transformed, index=X.index.to_list(), columns=self.column_names)
        return_1 = X.copy()
        return_1.drop(self.columns, axis=1, inplace=True)
        result = pd.concat([return_1, X_transformed], join="inner", axis=1) 
        return result

    def get_feature_names(self):
        return self.columns_names
    
    def get_feature_names_out(self):
        """get feature names of transform for sklearn compatibility > 1.0.0 """
        return self.column_names


# deprecated version
class Dummyencoder_deprecated(BaseEstimator, TransformerMixin):
    def __init__(self,columns, drop_first=True):
        self.categories = None
        self.drop_first = drop_first
        self.columns=columns

    def fit(self, X, y=None):
        s = self.drop_first
        self.categories = pd.get_dummies(X, drop_first=s).columns.to_list().copy()
        self.columns_names = self.categories
        return self

    def transform(self, X):
        s = self.drop_first
        c = self.categories

        X_transform = pd.get_dummies(X, drop_first=s).copy()
        transform_columns = X_transform.columns.to_list().copy()

        matching_columns = set(c).intersection(set(transform_columns))
        result = X_transform.loc[:, matching_columns].copy()

        if len(matching_columns) != len(c):

            for i in range(0, len(c)):
                if c[i] not in result.columns.to_list():
                    result[c[i]] = 0

        result = result.loc[:, c]  # bring columns in right order
        del X_transform, transform_columns, matching_columns

        return result

    def get_feature_names(self):
        return self.columns_names
    
    def get_feature_names_out(self):
        """get feature names of transform for sklearn compatibility > 1.0.0 """
        return self.column_names
