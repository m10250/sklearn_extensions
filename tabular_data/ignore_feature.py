import pandas as pd
import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin

class ignoreFeature(BaseEstimator, TransformerMixin):
    """
    Drops column names from the data
    """
    def __init__(self, names):
        self.names=names

    def fit(self, X, y=None):
        X_data=X.copy()
        X_data.drop(self.names, axis=1, inplace=True)
        self.columnnames = X_data.columns.to_list()
        return self

    def transform(self, X):
        X_data = X.copy()
        X_data.drop(self.names, axis=1, inplace=True)
        return X_data

    def get_feature_names(self):
        return self.columnnames
    
    def get_feature_names_out(self):
        """get feature names of transform for sklearn compatibility > 1.0.0 """
        return self.column_names


class selectfeature(BaseEstimator, TransformerMixin):
    """
    Select data with column names
    """
    def __init__(self, names, return_series=False):
        self.names=names
        self.return_series = return_series

    def fit(self, X, y=None):
        X_data=X[self.names].copy()
        return self

    def transform(self, X):
        X_data = X[self.names].copy()

        if (len(self.names) == 1) and (self.return_series==True):
            X_data = X_data.iloc[:,0]
        elif isinstance(X_data, pd.core.series.Series):
            X_data = X_data.to_frame()
        return X_data

    def get_feature_names(self):
        return self.names
