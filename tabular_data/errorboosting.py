# modeling apporach after Naumzik, C., & Feuerriegel, S. (2020, April). One Picture Is Worth a Thousand Words? The Pricing Power of Images in e-Commerce. In Proceedings of The Web Conference 2020 (pp. 3119-3125).
# Paper: https://www.research-collection.ethz.ch/bitstream/handle/20.500.11850/423144.1/1/3366423.3380086.pdf

from sklearn.base import BaseEstimator, RegressorMixin
from copy import deepcopy

# add internal check

class ErrorBoostedRegression(BaseEstimator, RegressorMixin):
    """
    Boosting a first model by learning to predict the errors of the first one by using new features - the predicted errors are included in the first model type as a final model
    obj: Objects - Pipelines or models or Optuna Grid Search - should follow the sklearn convention of fit/predict
    Eitherway include feature selection of first and second stage model in the pipeline or attach all data in fit and select by feature names
    first_stage_features: Feature names to select for first stage 
    second_stage_features: Feature names to select for second stage

    """
    def __init__(self, obj:list, feature_name = "second_stage_prediction", first_stage_features=None, second_stage_features=None):
        self.obj = obj
        self.first_stage_features = first_stage_features
        self.second_stage_features = second_stage_features
        self.feature_name = feature_name
    
    def fit(self, X,y):
        # prepare models
        if len(self.obj)==3:
            pass
        else:
            self.obj.append(deepcopy(self.obj[0])) # in case just two pipelines/objects or models are passed append final stage regressor
        
        # first stage 
        first_stage = self.obj[0]
        
        if self.first_stage_features==None and self.second_stage_features==None:
            first_stage.fit(X,y)
            self.obj[0] = first_stage
            internal_prediction = self.obj[0].predict(X)
        else:
            X_first = X.loc[:, self.first_stage_features].copy()
            first_stage.fit(X_first,y)
            self.obj[0] = first_stage
            internal_prediction = self.obj[0].predict(X_first)
        
        error_ = internal_prediction - y

        # second stage

        second_stage = self.obj[1]

        if self.first_stage_features==None and self.second_stage_features==None:
            second_stage.fit(X,error_)
            self.obj[1] = second_stage
            internal_prediction_secnd = self.obj[1].predict(X)
        else:
            X_second = X.loc[:, self.second_stage_features].copy()
            second_stage.fit(X_first,error_)
            self.obj[1] = second_stage
            internal_prediction_secnd = self.obj[1].predict(X_second)

        # third stage
        third_stage = self.obj[2]
        X_final = X.copy()
        X_final[self.feature_name] = pd.Series(internal_prediction_secnd, index=X_final.index.to_list())


        if self.first_stage_features==None and self.second_stage_features==None:
            third_stage.fit(X_final,y)
            self.obj[2] = third_stage
        else:
            third_stage.fit(X_final,y)
            self.obj[2] = third_stage

        return self
    
    def predict(self, X,y=None):
        # first stage 
        #first_stage = self.obj[0]
        
        if self.first_stage_features==None and self.second_stage_features==None:
            internal_prediction = self.obj[0].predict(X)
        else:
            X_first = X.loc[:, self.first_stage_features].copy()
            internal_prediction = self.obj[0].predict(X_first)
        
        #error_ = internal_prediction - y

        # second stage

        #second_stage = self.obj[1]

        if self.first_stage_features==None and self.second_stage_features==None:
            internal_prediction_secnd = self.obj[1].predict(X)
        else:
            X_second = X.loc[:, self.second_stage_features].copy()
            internal_prediction_secnd = self.obj[1].predict(X_second)

        # third stage
        #third_stage = self.obj[2]
        X_final = X.copy()
        X_final[self.feature_name] = pd.Series(internal_prediction_secnd, index=X_final.index.to_list())


        if self.first_stage_features==None and self.second_stage_features==None:
            prediction=self.obj[2].predict(X_final)
        else:
            prediction=self.obj[2].predict(X_final)

        return prediction
