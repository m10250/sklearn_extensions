import pandas as pd
from sklearn.preprocessing import OrdinalEncoder
from sklearn.base import BaseEstimator, TransformerMixin

class Ordinalscaler(BaseEstimator, TransformerMixin):
    """
    #Dummy coder transforming features; based on OrdinalEncoder
    #Returung dataframe with names

    """

    def __init__(self,ordinalencoder=OrdinalEncoder(),columns=None, **kwargs):
        self.ordinalencoder = ordinalencoder
        self.columns=columns

    def fit(self, X,y=None):
        X_data = X.copy()
        self.columnnames = X_data.columns.to_list()
        if self.columns is not None:
            X_data = X_data[self.columns].copy()
            self.namefeature = X_data.columns.to_list()
        self.ordinalencoder_fit = self.ordinalencoder.fit(X_data,y)
        return self

    def transform(self, X):
        X_data = X.copy()
        if self.columns is not None:
            X_data = X_data[self.columns].copy()
        transformer = self.ordinalencoder_fit
        transformed_data = transformer.transform(X_data)
        # build output
        transformed_data = pd.DataFrame(transformed_data, columns=self.namefeature, index=X_data.index)
        if self.columns is not None:
            X_data = X.copy()
            X_data.drop(self.columns, axis=1, inplace=True)
            X_data.loc[:,transformed_data.columns]=transformed_data.copy()
            transformed_data = X_data.copy()
        return transformed_data

    def get_feature_names(self):
        return self.columnnames
    
    def get_feature_names_out(self):
        """get feature names of transform for sklearn compatibility > 1.0.0 """
        return self.column_names

class ownOrdinalScaler(BaseEstimator, TransformerMixin):
    """
    transformation is dict containing information with transformations
    """
    def __init__(self, transformation, baselevelname=None, baselevel=0):
        self.transformation=transformation
        self.baselevelname = baselevelname
        self.baselevel = baselevel

    def fit(self, X, y=None):
        self.column_names = X.columns.to_list()
        return self

    def transform(self, X):
        X_data = X.copy()

        for key in self.transformation.keys():
            temp_dict = self.transformation[key]
            if self.baselevelname is not None:
                X_data[key]=X_data[key].map(lambda x: temp_dict[x] if x in temp_dict.keys() else temp_dict[self.baselevelname]).copy()
            else:
                X_data[key] = X_data[key].map(
                    lambda x: temp_dict[x] if x in temp_dict.keys() else self.baselevel).copy()
        return X_data

    def get_feature_names(self):
        return self.column_names
    
    def get_feature_names_out(self):
        """get feature names of transform for sklearn compatibility > 1.0.0 """
        return self.column_names
