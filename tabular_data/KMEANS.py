import pandas as pd
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.cluster import KMeans

class KMEANS(BaseEstimator, TransformerMixin):
    """
    Kmeans trransformer for transforming high dimensional data into k clusters (which can be used in dummy coding; dummy coder needs to be added afterwards)

    name: Name of the return feature
    model: sklearn KMeans model
    return_str: if return variable should string (True) or integer
    """
    def __init__(self,name, model=KMeans(),return_str=False, **kwargs):
        self.model = model
        self.name = name
        self.return_str = return_str

    def fit(self, X, y=None):
        self.kmeans_fit = self.model.fit(X)
        return self

    def transform(self,X):
        if self.return_str == True:
            type = str
        else:
            type = None
        prediction = self.kmeans_fit.predict(X)
        prediction = pd.DataFrame(prediction,columns=[self.name], index=X.index, dtype=type)
        return prediction

    def predict(self, X):
        if self.return_str == True:
            type = str
        else:
            type = None
        prediction = self.kmeans_fit.predict(X)
        prediction = pd.DataFrame(prediction, columns=[self.name], index=X.index, dtype=type)
        return prediction

    def get_feature_names(self):
        return [self.name]
    
    def get_feature_names_out(self):
        """get feature names of transform for sklearn compatibility > 1.0.0 """
        return [self.name]

    # TODO Optimize parameter
